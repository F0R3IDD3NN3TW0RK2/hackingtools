#!/usr/bin/env python


from dcim.models import Device
from extras.scripts import Script


class CAssignTenantToDevice(Script):
    description = "Assign Tenant to Device"

    def run(self, data):
        Vget_devices = Device.objects.all()
        for device in Vget_devices:
            # Check if device is racked,
            if device.rack is not None and device.tenant is not None:
                if device.name is not None:
                    self.log_info("Device Name: {}".format(device.name))
                else:
                    self.log_info("Device Name: None ")
                self.log_info("Rack Before Tenant Change {}".format(device.tenant))
                device.tenant = device.rack.tenant
                self.log_info("Rack After Tenant Change Tenant {}".format(device.tenant))
                device.save()
            else:
                continue